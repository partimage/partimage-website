+++
weight = 120
title = "Download"
nameInMenu = "Download"
draft = false
aliases = ["/download"]
+++

## General installation information

Partimage is only available for linux. If you do not have linux installed on
your computer, you can use it by booting a system rescue live system such as
[SystemRescue](https://www.system-rescue.org/). If you already have linux
installed on your system, it is recommended to install partimage using the
package management system that comes with your linux distribution.

## General download information

You can also download and compile partimage from sources but it requires more
effort and there several libraries will be required.

| Release         | partimage-0.6.9                                                                                                                            |
|:---------------:|:-------------------------------------------------------------------------------------------------------------------------------------------|
| Date            | 2010-07-25                                                                                                                                 |
| Sources files   | [partimage-0.6.9.tar.bz2](https://gitlab.com/partimage/partimage-sources/uploads/6f5867515e3267bad0e70b75410941f4/partimage-0.6.9.tar.bz2) |
| SHA256 checksum | 753a6c81f4be18033faed365320dc540fe5e58183eaadcd7a5b69b096fec6635                                                                           |

## How to compile partimage from sources

Once you have downloaded partimage-x.y.z.tar.bz2, you must extract the archive
and compile it:

* Here is how to extract the sources: `tar xfjp partimage-x.y.z.tar.bz2`
* To compile it: `./configure --prefix=/usr && make && make install`
