+++
title = "Installation"
draft = false
aliases = ["/Partimage-manual_Installation"]
+++

## How to obtain Partimage

To obtain the last version of Partition Image, you can visit the
[download page](/Download). You can download sources in tar.bz2 format,
or in binary formats. This page explain how to install Partiamge from sources.

## Requirements

Many libraries are required to compile/run Partition Image:

* libz gzip compression support
* libbz2 bzip2 compression support
* liblzo lzo compression support
* libnewt GUI (Graphical User Interface)
* libslang Required for newt
* libssl OpenSSL: encryption of data over the network
* libcrypt support for password

You can go on the download section of the home-page of Partition Image
to find more details about these libraries: where to download them, and
how to compile them.

**Warning:** If you want to compile partimage as a shared program, you
need to have all the shared version of these libraries installed on your
system. If you want to compile partimage as a static program, you need
to have all the static version of these libraries installed on your
system.

## Compilation and installation

### Different versions of Partition Image

There are 3 versions of Partition Image you can use: The source code you
have to compile it, and it may be difficult, because it require many
libraries The static binary this is very easy to use: this is an i386
executable binary, you just have to run it. The boot/root disks if you
don't have an installed linux system, or if you want to work on your
root linux partition The bootable eltorito cdrom you can run partimage
from a bootable cdrom, where the image files are burned

All these versions can be downloaded from the web site. Here, we will
explain how to compile the source code:

## Compiling Partition Image sources

In order to compile and install PartImage on your system, type the
following in the base directory of the PartImage distribution:

`./configure`\
`make`\
`make install`

After this, if you enabled SSL, you can install the needed certificate
with

`make certificates`

Since PartImage uses autoconf you should have not trouble compiling it.
