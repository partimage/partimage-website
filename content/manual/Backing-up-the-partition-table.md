+++
title = "Backing up the partition table"
draft = false
aliases = ["/Partimage-manual_Backup-partition-table"]
+++

## Introduction to partitions

Even if you have only one volume, your hard disk is divided into
partitions. The standard MSDOS volume label allows to have up to 4
primary partitions. The partition table is stored in the MBR (Master
Boot Record). This area is the first 512 bytes of the physical hard
drive. It also contain some code which will start the operating system,
which can be the bootloader (LILO, grub, ...). If your hard disk is hda,
primary partitions are hda1, hda2, hda3 and hda4. It's easy to save to
primary partitions table, by copying the MBR.

To get past this limitation of 4 primary partitions, you can create an
extended partition. An extended partition is a primary partition which
contains a lot of partitions. For example, if hda2 is an extended
partition, its logical partitions will appear as hda5, hda6, hda7, ...
even if you don't use 4 primary partitions. That's why you can have an
hda5 device, with no hda4. The problem with extended partition is there
is no table we can easily save. The extended partition contains a linked
list. hda5 will point to hda6, hda6 will point to hda7 and so on. This
makes it difficult to save the partition table of the extended
partition.

Partimage can save the data of one partition, but it won't save your
partition table. If you have a major problem with your hard drive, you
may have to restore both the partition table and the data - having the
images of the partitions won't be a lot of help on there own. Which is
why we'll show you how to save the partition image now.

## Making a backup of the partition entries

We will save all the partitions entries (both primary and logical ones
which appear in the extended partition). In this example, we'll be
assuming that hda (the first IDE hard disk) is to be backed up.

First, we will save the MBR with DD (GNU convert and copy)

`cd /root`\
`mkdir partition-backup`\
`cd partition-backup`\
`dd if=/dev/hda of=backup-hda.mbr count=1 bs=512`

It will produce a very small, but very important file: 512 bytes of
data. Now, we will save entries of the extended partitions:

`sfdisk -d /dev/hda > backup-hda.sf`

sfdisk is a tool provided with the
[util-linux package](http://www.kernel.org/pub/linux/utils/util-linux/).

**IMPORTANT:** You should now put these files somewhere safe - copy them
to a floppy disk (and take a copy of it!), or burn them onto a CD. Keep
these files safe. Do not leave them on your hard drive - if there is a
problem with th drive, you may not be able to access these files, and
while your partition images won't be wortheless, it will certainly be a
lot harder to restore your data. Ideally you want to have multiple
copies in different locations.

## Restoring partition entries from the backup

Be careful, restoring is a dangerous action - it can destroy data!
First, we will restore the Master Boot Record:

`dd if=backup-hda.mbr of=/dev/hda`

Then, here is how to restore extended partitions entries:

`sfdisk /dev/hda < backup-hda.sf`

To finish, you will have to reboot your computer.
