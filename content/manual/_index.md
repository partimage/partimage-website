+++
weight = 150
title = "Partimage manual"
nameInMenu = "Manual"
draft = false
aliases = ["/Partimage-manual"]
+++

Here is the official documentation for Partimage. It is recommended to read the
following pages in the standard order.

* [Installation](/manual/Installation/)
* [Basic usage](/manual/Basic-usage/)
* [Network support](/manual/Network-support/)
* [Backing up the partition table](/manual/Backing-up-the-partition-table)
