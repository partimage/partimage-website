+++
weight = 110
title = "Screenshots"
nameInMenu = "Screenshots"
draft = false
+++

The following screenshots gives you an idea of the semi-graphical interface
provided with partimage and it also shows the main screens you will have to use:

![partimage-001](/images/screenshot-001.png)

![partimage-002](/images/screenshot-002.png)

![partimage-003](/images/screenshot-003.png)

![partimage-004](/images/screenshot-004.png)

![partimage-005](/images/screenshot-005.png)
