+++
weight = 160
title = "Frequent Questions"
nameInMenu = "FAQ"
draft = false
aliases = ["/Partimage-FAQ"]
+++

## General questions

#### If I make an image file of a partition, can I restore it to an unpartitioned hard disk ?

The current version can only read/write from/to a partition. Partimage
doesn't use the partition table at all. (The Linux kernel reads it, and
Partimage can directly use hda1, hda2, ...). But I'd like to implement
this in the future, to allow users to select partition to save/restore
from a list (with name, size, filesys, usage, ...), and Partimage would
be able to create the partition if needed at restore, or resize it...

#### Can I restore it to a smaller or bigger partition ?

You can't restore to a smaller partition (you will have an error), but
it's possible to restore to a lager one. In this case, some space will
be lost (I suppose the OS cannot use all the size). Partimage don't have
a resize feature, but you can use other tools. I'd like to add this in
the future too. It will allow to restore into a smaller or larger
partition. Indeed, as Partimage is low level it uses data blocks. So
resizing is possible, but that's a complex feature to implement. With
some File Systems made to be easily resizable (as NTFS, ext2, ReiserFS),
it may be easy, but with FAT, it's hard to do. For example, when
resizing from 1,5 GB to 3 GB, you must change FAT16 into FAT32... You
can use GNU Parted to do it.

#### I need a write support for NTFS. It exists in kernel-2.4, but it's not enabled in partimage bootdisk. How can I use it ?

NTFS write support, as UFS write support are dangerous and supported
only by experimental drivers. It can damage partitions, and corrupt
data. That's why it's not enabled in partimage-bootdisk. Users who
weren't aware of this problem could lose their data if the support was
enabled.

If you need the write support, you will have to use another bootdisk.
Please, have a look at the question in this FAQ which explains how to
build your own bootdisk.

#### Partimage says “Segmentation Fault”. What does this mean and how can I fix this problem?

A segmentation fault means that some part of the program was not coded
correctly. Report this to authors. Give as many details as you can
(architecture, package version, what you did, /proc/partitions file,
output of dmesg, logfiles...). If you can reproduce the matter, tell us
what you did step by step. In any case, if you can, try with -g10 option
to create full log file. We may need your help so don't delete images or
don't erase your partition.

#### I have a non i386 architecture machine, will partimage/partimaged work on it?

Quick answer: no. A bit longer, partimage/partimaged 0.6.x can't work on
Big Endian machine. On Little Endian machine, you may have luck any make
it working but you'll be on your own because we won't be able to help
you. If someone has an old Sparc, HPUX, MIPS or whatever he doesn't use,
please, give it to us and we'll be able to make partimage working on
non-i386.

#### Is there a batch mode to use partimage in a script?

Yes. You just need to use -b (batch) option on command line. You may use
something like this: partimage -b -z3 save /dev/sda9 /mnt/image\_sda9.

## Partimage vs others

#### What does partimage give you over the following: clear the free blocks with DD, and copy with DD

`dd if=/dev/zero of=/mnt/filetmp`\
`rm /mnt/filetmp `\
`dd if=/dev/xxx`\
`gzip > image_file.gz`

1.  Partimage is faster. You don't have to wait for “dd if=/dev/zero”
    first. during the copy, free blocks are not read. Then, if 20 % of
    the partition is used, partimage will avoid two access to 80 % of
    the free areas.
2.  There's a GUI (graphical user interface). It has a lot of
    advantages: you can see the remaining time, the percentage of the
    copy, ... The Qt GUI in the next version will be very nice and easy
    to use.
3.  Partimage can work on file systems which are not supported (stable
    write support) by the Linux kernel, such as NTFS, BSD ffs, XFS/JFS
    in a non-patched kernel. To run “dd if=/dev/zero”, you need the
    write support in Linux.
4.  Partimage is made to be easy to use, and to replace commercial
    software such as Ghost, Drive Image, ... and the user does not have
    to know many command lines.
5.  Partimage has a lot of options, such as -V which allow th create a
    new volume if space is missing. DD will show and error and abort.
6.  The network support allows to save an image file from a client,
    without having to configure both client and server NFS. (Network
    File System). When the multicast will be implemented, it will allow
    to restore X clients from 1 server, and DD can't do a such multicast
    copy.
7.  We provide rootdisk and bootcd. You can boot on it, if Linux is not
    installed on your computer. They contains everything that is need
    (the LZO compression in 0.7, which is very useful for big files).
    You don't have all these tools on every boot rescue systems.

#### Any hope of a browser like utility to allow restoring individual files from the archives? Like Ghost Explorer ?

Sorry, Partition Image is not a high level program, like Ghost. Ghost
makes a file copy, and we obtain a file in a tar.gz archive, with other
details (such as the boot sector). But Partimage works at a low level,
as Drive Image: it copies used blocks of the disk. So, it doesn't know
where files are written, and cannot allow to see files in the image
without restoring. But maybe it will become possible in future versions.
We will try to make partimage image files mountable: a driver, such as
the loop one, could allow us to make an image file be a block device. If
you know how to write block device drivers, please contact us.

## About boot systems

#### A driver is missing in the partimage-bootdisk/bootcd. How can I solve the problem ?

Many users write us because they can't use the network (or another
device). In most cases, the problem comes from the bootdisk/bootcd which
does not support all drivers.

Space is limited in both partimage-bootdisk and partimage-rootdisk. We
can't include all drivers that exists in the kernel sources. That's why
only the most common NE2000 cards, and the most common AIC7xxx SCSI
drivers are supported. It provides support for a lot of devices, but
some of them need another driver. For example, 3COM cards are not
supported. There are many ways to solve this problem:

-   You can also **replace partimage-bootdisk with a slackware
    bootdisk**. Slackware provide a lot of bootdisks, and each of them
    include a kind of drivers. For example, there's one with all the
    SCSI drivers, another with all network drivers, and another with USB
    support. You can choose the one which will support your device. The
    full list of bootdisks of Slackware 8 can be find here:
    <ftp://ftp.slackware.com/pub/slackware/slackware-8.0/bootdsks.144/>.
-   You can **compile your own kernel**. That's the best way to be sure
    all your devices will be supported. We won't tell you how to compile
    your kernel here. We will tell you how to use a compiled
    vmlinuz/bzImage to create a partimage bootdisk. But first, when
    compiling your kernel, you must include some options: In the
    block-devices page, you must choose Yes for the last four options:
    -   Loopback device support
    -   Network block device support
    -   RAM disk support with a default size of 40960 (that's not an
        error) disk size

Initial RAM disk initrd support. In the File systems, you must choose
Yes for the Minix FS support. In the loadable module support page, you
must choose No, in order to disable modules. After compiling your
kernel, you will obtain bzImage. You must type three commands to make it
the same thing as an official partimage-bootdisk image:

-   -   rdev bzImage /dev/fd0
    -   rdev -r bzImage 49152
    -   rdev -R bzImage 0

#### When I use the bootdisk, I see an error with 09000900090009000900090009000 on the screen

I'm having a problem with booting from the bootdisk.

I've created the disk from the partimage-0.3.5-bootdisk-1.raw file, and
when booting, all I see is

090009000900090009000900090009000900090009000900090009000900090009000900090
0090009000900090009000900090009000900090009000900090009000900090009000900090
0090009000900090009000900090009000900090009000900

all over the screen.

This is a problem when you download the file with Netscape, and the
ASCII mode. Try using FTP, or another tool: type wget
AdressOfTheFileToDownload

## Running partimaged

#### Where does partimaged save images?

The path is the one you choose with partimage in “Image file to
create/use” on first screen. 2 cases: 1. If you entered absolute path
(name starts by a '/'), it will be used as it. 2. You entered a relative
path (name starts by anything else than '/'). The path will be related
to the one you started partimaged from. If you only gave image filename,
the path will be the current directory (where you started partimaged).

#### Partimaged says “Write access is denied”. What's wrong?

partimaged switched to 'partimag' user for security reasons but ion your
system, this user can't write in the directory you choose to save
images. You have to change permissions of the directory. Man chmod and
chown can help you here.

#### How do I generate certificate/key file for SSL support

First: run

-   openssl req -new -x509 -outform PEM &gt; partimaged.csr
-   openssl rsa -in privkey.pem -out partimaged.key
-   openssl x509 -in partimaged.csr -out partimaged.cert -signkey
    partimaged.key

Then, put partimaged.key and partimaged.cert into appropriate directory
and **chmod them 0600**

#### How to run partimaged in chroot environment

-   Create a etc subdirectory in the chroot directory and put files in
    it:
    -   /etc/ld.so.conf
    -   /etc/ld.so.cache
    -   /etc/passwd
    -   /etc/shadow
    -   directory /etc/terminfo
-   Create a lib subdirectory and put files into it:
    -   /lib/libc.so.6 and its linked file
    -   /lib/ld-linux.so.2 and its linked file
    -   /lib/libnsl.so.1 and its linked file
    -   /lib/libnss\_compat.so.2 and its linked file
-   Create a dev subdirectory and run mknod tty c 5 0 in it.
-   If needed, also put partimagedusers, partimaged.key and
    partimaged.cert into appropriate location.

If partimaged --chroot {your directory} doesn't fail, you'll be able to
run partimaged as daemon into chrooted environment with \`partimaged -D
--chroot {your directory}'

## Networking questions

#### Will it be possible to save images to a Windows(TM) shared directory?

Not yet. Once we'll have a working boot cdrom we'll try to add this
feature via Samba.

## New file systems

#### I use XFS. Does partimage also take things like ACLs and other Extended Attributes into account ?

Yes. Partimage makes a physical copy of the partition. It's not a simple
“tar.gz” of files. In other words, it does the same work than dd does,
but it forget to copy all free blocks in order to save space and time.
All blocks which are not marked as free will be copied. Then everything
is kept (boot sector informations, all attributes that can exist, ...)
It even keeps location of files on the disk, then the vmlinuz file won't
be moved, and LILO will continue to work.

## ChangeLog

-   revision 1.1
    -   Initial release.
-   revision 1.2
    -   Spelling checked.
-   revision 1.3
    -   New section: partimage in batch mode.
    -   Some sections reorganized.

## Copyright

Copyright 2002-2003 François Dupoux, Franck Ladurelle.

We disclaim everything. The contents of this article might be totally
inaccurate, inappropriate, misguided, or otherwise perverse.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.1 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts, and with no Back-Cover
Texts.

Note: it's not a call to make this document unusable .
