+++
weight = 100
title = "Partimage Homepage"
nameInMenu = "Homepage"
draft = false
aliases = ["/Main_Page","/Page_Principale",]
+++

## About partimage

Partimage is **opensource disk backup software**. It saves partitions
having a [**supported filesystem**](/Supported-Filesystems/) on
a sector basis to an image file. Although it runs under Linux, Windows
and most Linux filesystems are supported. The image file can be
compressed to save disk space and transfer time and can be split into
multiple files to be copied to CDs or DVDs.

Partitions can be [**saved across the network**](/manual/Network-support/) using
the partimage network support, or using Samba / NFS (Network File Systems). This
provides the ability to **perform an hard disk partition recovery** after a disk
crash.

**Partimage will only copy data from the used portions of the partition.** (This
is why it only works for [**supported filesystem**](/Supported-Filesystems/).
For speed and efficiency, free blocks are not written to the image file. This is
unlike other commands, which also copy unused blocks. Since the partition is
processed on a sequential sector basis disk transfer time is maximized and seek
time is minimized, Partimage also works for very full partitions. For example, a
full 1 GB partition may be compressed down to 400MB.

**Partimage is no longer an active project.** It is recommended to use alternatives
such as [fsarchiver](https://www.fsarchiver.org/), [partclone](https://partclone.org/),
or filesystems tools such as xfsdump.

## When to use it

-   Very useful to restore partitions for recovery if there is a problem
    like virus infection, file system errors, upgrade regression errors,
    configuration error or hard drive error or failure. Partimage is
    **fast and easy** to use, create and image of the system partition
    often. When you have a problem, just restore the partition, and
    after a few minutes, you have the original partition. No need to
    reconfigure your system, utilities, daemons or reinstall and taylor
    applications. You can write the image to a CD-R for archival
    purposes or to avoid using hard-disk space.
-   **Installing many identical computer images**. For example, if you
    buy 50 PCs, with the same hardware, and want to install the same
    system on all of then, you will save a lot of time. Install on the
    first PC and create an image from it. For the 49 others, you can use
    the image file and Partition Image restore function.
-   An other example is for **instructional use** where all students
    need to have the same environment to start with. At the end of a
    session each student can write a CD ( or save their system partition
    to a fileserver, students frequently forget or misplace this type of
    thing) with their current revisions. At the beginning of the next
    session, restore the image and they can continue. They may
    misconfigure or corrupt their environment in the course of their
    learning they will be able to “step back” to a well defined point
    where their environment was working.

## Limitations

Partimage does not support **ext4** or **btrfs** filesystems.

No defragmentation is performed during save or restore.

Filesystem being backedup must be unmounted and inaccessible to other
programs. This means that when backing up the “system” filesystem, the
operating system is down.

Single files or directories cannot be restored. Although the backup
could be restored to an temporary/alternate partition and single files
or directories could be recovered from the alternate partition.

## Advantages

**Fast**. Since file fragments are not “hunted down” head movement is
minimized allowing optimal use of the disk's cache.

**Fast**. Since only allocation clusters which are in use as read and
saved.

**Read only**. None of the data being backed up is modified. This
includes meta-data like last-access date. Preserves integrity of source
partition.

**Multi file synchronization**. If there are multiple files that make up
a fileSet, no file in the set will be out of sync with the other files.

**Destination flexibility**. Backup media can be a file on another
partition on the same disk, another local disk or a network attached
file.

**Fast Restore**. Data is simply flowed onto the disk optimizing the
disk cache and minimizing disk seek time.

**Simple restore**. There is only one media to restore. No decision
regarding order of restoring multiple back media.

**No recover of deleted files**. Files that were deleted at the time of
the backup are not on another media that needs to be restored causing
deleted files be resurrected

**recovery to alternate partition** Excellent choice for mirroring the
file system on another disk or at another location.

## Other information

For a utility for backing up a filesystem on a file basis with more
features such as file exclusion, checksumming, multi-thread compression,
encryption you should use [FSArchiver](http://www.fsarchiver.org).
