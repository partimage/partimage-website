+++
weight = 140
title = "Supported filesystems"
nameInMenu = "Filesystems"
draft = false
+++

Since partimage copies only filesystem used block, it must know the layout
of the filesystem, and cannot save unsupported filesystems. Here is the list of
the filesystems currently supported:

| Filesystem  | Description                                      | State       |
|:-----------:|:-------------------------------------------------|:-----------:|
| ext2/ext3   | Old versions of the default Linux filesystem     | stable      |
| xfs         | Primary Linux filesystem                         | stable      |
| ext4        | Current default Linux filesystem                 | unsupported |
| btrfs       | Primary Linux filesystem                         | unsupported |
| reiserfs-3  | Alternative journalized file system              | stable      |
| reiserfs-4  | Alternative journalized file system              | unsupported |
| fat16/fat32 | DOS and old Windows file systems                 | stable      |
| HPFS        | IBM OS/2 File System                             | stable      |
| JFS         | Alternative journalized file system              | stable      |
| UFS         | Unix File System                                 | beta        |
| HFS         | Old MacOS File System                            | beta        |
| NTFS        | Windows NT, 2000 and XP                          | experimental|

**The NTFS (Windows NT File System) is currently not fully supported:** this
means you will be able to save an NTFS partition if system files are not very
fragmented, and if system files are not compressed. In this case, you will be
able to save the partition into an image file, and you will be able to restore
it after. If there is a problem when saving, an error message will be shown and
you won't be able to continue. If you have successfully saved an NTFS partition,
you shouldn't have problems as you restore it (except in case of bugs). Then the
best way is to try to save a partition to know if it is possible. If not, try to
defragment it and try to saving the partition again.
